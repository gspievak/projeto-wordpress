<?php 
    /*
        Template Name: Posts
    */
?>

<?php get_header() ?>

    <main> 
        <h1>ola aqui é a post.php</h1>

        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <h2><?php the_content() ?></h2>

        <?php endwhile; else : ?>
	        <p><?php esc_html_e( 'não encontramos nenhum post para você' ); ?></p>
        <?php endif;
            echo paginate_links();
        ?>

    </main>

<?php get_footer() ?>