<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'F6yuD2UTVfUZymdnal/8DseQ6es/mjZJseJ3oMCEWcQgZHvuAtq5teQdJoOPibnOXID4SdmYYsYlx+ZLBXxAaw==');
define('SECURE_AUTH_KEY',  'G74ko5+yFGzBAXYf3P8h4Jf/jUr5aeAROzEA/reHf1vSdWeIZdJ/m4DgYJLsJ/nkGJsOxOGSp0MwUX1aSyF3jw==');
define('LOGGED_IN_KEY',    'HuCU/GM19mmKhrptlnuC5Vil6MGGYZL/l1S4dguW6ei/VvKEWiU2Uh/r3bC/AbKKR6LgAepPkNwS5GyoVbQBPA==');
define('NONCE_KEY',        'z5g2ABpOv3q3Cq3MHCAwfhbrOsN+m3DXA6voDXh1oRdEOqUXhZbSkDdiEDu4c9t65XbeCEXaNDbqSZ32eD5UEA==');
define('AUTH_SALT',        'yPQ+l63pzStPrDvhdJLPzyABZF8RV3upzVWkWeIZxxzhkSv1WMqaJo41SlAj/NTsc3jqMiQktf3TjP2+vr/VYg==');
define('SECURE_AUTH_SALT', 'VT6roDUF9owEfIjbw2HmTqVq8GplMbxzIyR1mPHehbAESkM/fRChh5/Nxs/uy3NK9aunRBk+UbaXWhH45xDQLQ==');
define('LOGGED_IN_SALT',   '2DRbuZSYICqQ6MH/2wnWNhezl3j7CCpO6WosdtThmGVDL9gVQBxQHKMxlCTy0Xgdv5udyj8Qr5nVKBhptn5EEQ==');
define('NONCE_SALT',       'kaOhjzAVsGszs2e489irRNtZieZ7hxLbUx8GyEtB1cVp3u+4E2EfUdFFQaWIJ5YkmG7D8lQGxUUmykdvTE4Mlg==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
